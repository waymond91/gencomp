import unittest
from OpenReadingFrames import OpenReadingFrames, genbank_orf_fetch, genbank_download
from Bio.Seq import Seq

class MyTestCase(unittest.TestCase):
    example_start = 'ATG'
    example_stops = ['TGA', 'TAA', 'TAG']
    test_sequence = Seq("ATGCTACCCTGACAT")
    #                    012345678901234

    def test_inputs(self):

        self.assertRaises(TypeError, OpenReadingFrames, "ATCGAATCGATT", \
                          self.example_start, self.example_stops)
        self.assertRaises(TypeError, OpenReadingFrames, self.test_sequence, \
                          True, self.example_stops)
        self.assertRaises(TypeError, OpenReadingFrames, self.test_sequence, \
                          self.example_start, "TAG")
        self.assertRaises(TypeError, OpenReadingFrames, self.test_sequence, \
                          self.example_start, [1,2,3])
        self.assertRaises(TypeError, genbank_orf_fetch, 123)
        self.assertRaises(TypeError, genbank_download, True)

    def test_outputs(self):
        orfs_handle = OpenReadingFrames(self.test_sequence, self.example_start, self.example_stops, min_length=1)
        self.assertEqual(len(orfs_handle.orfs), 2)
        self.assertEqual(orfs_handle.orfs[0]['seq'],Seq("ATGCTACCCTGA"))
        self.assertEqual(orfs_handle.orfs[1]['seq'],Seq("ATGTCAGGGTAG"))
        self.assertEqual(orfs_handle.orfs[0]['translation'],Seq("MLP*"))
        self.assertEqual(orfs_handle.orfs[1]['translation'], Seq("MSG*"))

        self.assertEqual(orfs_handle.find_all(self.test_sequence, "C"), [3, 6, 7, 8, 12])
        self.assertEqual(orfs_handle.find_all(self.test_sequence, "AT"), [0, 13])
        self.assertEqual(orfs_handle.find_all_multiples(self.test_sequence, ["C", "AT"]), \
                         {"C":[3, 6, 7, 8, 12], "AT":[0, 13]})

if __name__ == '__main__':
    unittest.main()
