import wget
import os
from Bio import SeqIO
from Bio.Seq import Seq
import numpy as np

class OpenReadingFrames:

    def __init__(self, seq, name, start_codon, stop_codons, min_length = 9):

        if type(seq) is not Seq:
            raise TypeError("seq should be of type biopython.Seq")

        if type(start_codon) is not str:
            raise TypeError("start_codon must be of type str.")

        if type(stop_codons) is not list:
            raise TypeError("Stops codons must be a  list of strings")

        if type(stop_codons[0]) is not str:
            raise TypeError("Stops codons must be a  list of strings")

        self.lengths = []
        self.orfs = []
        self.name = name
        self.seq = seq
        self.rev_seq = seq.reverse_complement()
        self.start_codon = start_codon
        self.stop_codons = stop_codons
        self.min_length = min_length
        self.get_orfs()
        self.orfs.sort(key=lambda x: x['length'], reverse=True)
        self.lengths.sort()

    # Index all instances of a sub sequence
    # Returns a list indexes
    def find_all(self, seq, sub_seq):
        sub_seq_len = len(sub_seq)
        result = seq.find(sub_seq)

        indexes = []
        while result != -1:
            if result != -1:
                indexes.append(result)
            result = seq.find(sub_seq, start=result + sub_seq_len)
            # print(result)
        return (indexes)

    # Index all instances of a list of sub sequences
    # Returns dictionary where the key is the sub sequence
    # and the value is a list of indexes
    def find_all_multiples(self, seq, sub_seqs):
        results = {}
        for sub_seq in sub_seqs:
            indexes = self.find_all(seq, sub_seq)
            results.update({sub_seq: indexes})
        return (results)

    # Find open reading frames in a sequence given
    # the stop and start codons passed to the function
    # Returns a list of tuples with start and stop locations
    def get_orf_indices(self, seq):
        orf = []
        start = seq.find(self.start_codon)
        start_codon_len = len(self.start_codon)
        while start != -1:
            stop_index = []
            for stop_codon in self.stop_codons:
                stop_index.append(seq.find(stop_codon, start=start + start_codon_len))
            stop_index = list(filter((-1).__ne__, stop_index))
            if len(stop_index) == 0:
                break
            stop = min(stop_index)
            if start != -1 and (stop - start) > self.min_length:
                orf.append((start+3, stop))
            last_start = start
            if stop != -1:
                start = seq.find(self.start_codon, start=stop + 3)
            else:
                break
            if (start < last_start):
                break
        return (orf)

    def get_counts(self, min_length, max_length):
        thresholds = np.linspace(min_length, max_length, 25)
        y = []
        x = []
        for threshold in thresholds:
            norm_orf = self.get_orf_indices(self.seq, threshold)
            revc_orf = self.get_orf_indices(self.rev_seq, threshold)
            y.append(len(norm_orf) + len(revc_orf))
            x.append(threshold)
        return (x, y)

    def get_orfs(self):
        norm_indices = self.get_orf_indices(self.seq)
        revc_indices = self.get_orf_indices(self.rev_seq)

        count = 0
        for index in norm_indices:
            new_seq = self.seq[index[0]:index[1]]
            count += 1
            if len(new_seq) % 3 == 0:
                new_frame = {
                    "seq": new_seq,
                    "translation": new_seq.translate(),
                    "rev": False,
                    "start_index": index[0],
                    "stop_index" : index[1],
                    "length": len(new_seq),
                }
                self.orfs.append(new_frame)
                self.lengths.append(len(new_seq))

        for index in revc_indices:
            new_seq = self.rev_seq[index[0]:index[1]]
            count += 1
            if len(new_seq) % 3 == 0:
                new_frame = {
                    "seq": new_seq,
                    "translation": new_seq.translate(),
                    "rev": True,
                    "start_index": index[0],
                    "stop_index": index[1],
                    "length": len(new_seq),
                }
                self.orfs.append(new_frame)
                self.lengths.append(len(new_seq))

def to_fasta(sequences, filename):
    if type(sequences) is not list:
        raise TypeError("sequences must be a  list of biopython.seq objects")

    if type(sequences[0]) is not Seq:
        raise TypeError("sequences must be a  list of biopython.seq objects")

    SeqIO.write(sequences, filename+".fasta", "fasta")


def genbank_orf_fetch(an, name, start_codon, stop_codons, dir="data"):
    if type(an) is not str:
        raise TypeError("Accession Number must be of type str.")

    genbank_base_url = 'https://www.ncbi.nlm.nih.gov/search/api/download-sequence/?db=nuccore&id='
    url = genbank_base_url + an
    file_path = dir +"/"+ an + '.fna'
    file_exists = os.path.isfile(file_path)
    if file_exists:
        filename = an + '.fna'
        print(filename+ " already exists, skipping.")
    else:
        filename = wget.download(url, out=dir)
        print("Succesfully downloaded: " + filename)

    fasta_sequences = SeqIO.parse(open(file_path), 'fasta')
    for fasta in fasta_sequences:
        seq = fasta.seq

    orfs = OpenReadingFrames(seq, name, start_codon, stop_codons)
    return(orfs)

def genbank_download(accession_number, dir="data"):
    if type(accession_number) is not str:
        raise TypeError("Accession Number must be of type str.")
    genbank_base_url = 'https://www.ncbi.nlm.nih.gov/search/api/download-sequence/?db=nuccore&id='
    url = genbank_base_url + an
    if not os.path.isfile(dir + an + '.fna'):
        filename = wget.download(url, out=dir)
        print("Succesfully downloaded: " + filename)
    else:
        filename = an + '.fna'
    filename = filename  # "/kaggle/working/"+filename
    fasta_sequences = SeqIO.parse(open(filename), 'fasta')
    for fasta in fasta_sequences:
        name, seq = fasta.id, str(fasta.seq)
        print(name, seq[:10] + "..." + seq[-10:])

    seq = Seq(seq)
    return (filename, seq)

def download_to_fasta(accession_numbers, dir="data"):
    sequences = []
    for an in accession_numbers:
        genbank_base_url = 'https://www.ncbi.nlm.nih.gov/search/api/download-sequence/?db=nuccore&id='
        url = genbank_base_url + an
        file_path = dir + "/" + an + '.fna'
        file_exists = os.path.isfile(file_path)
        if file_exists:
            filename = an + '.fna'
            print(filename + " already exists, skipping.")
        else:
            filename = wget.download(url, out=dir)
            print("Succesfully downloaded: " + filename)

        fasta_sequences = SeqIO.parse(open(file_path), 'fasta')
        for fasta in fasta_sequences:
            seq = fasta.seq
            sequences.append(fasta)

    SeqIO.write(sequences, "all_sars.fasta", "fasta")




