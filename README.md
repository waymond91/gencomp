# Genetic Sequencing
This project was a practice excercise to see if I could identify proteins in a raw genome.
Genetics has interested me for some time, and with the recent pandemic, it seemed like an appropriate time to do a little learning.
In these files I go through all of the excercises found in the textbook:  
*Computational Genomics: A Case Studies Approach* by Nello Cristianini.

Sars-Cov-2 actually makes for a great introductory case study, as it has a relatively small genome (a small mRNA virus), and is well documented.

![Jupyter Notebook Output](SARS-Cov-2.jpg)
