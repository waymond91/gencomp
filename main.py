# %%
import pandas as pd
from OpenReadingFrames import OpenReadingFrames, genbank_orf_fetch, download_to_fasta

# Use NCBI taxonomy browser to verify results
virus_table = pd.read_table('data/viruses.csv',delimiter=",")
start_codon = 'ATG'
stop_codons = ['TGA', 'TAA', 'TAG']

# %%
virus_orfs = []
accession_numbers = []

for index, row in virus_table.iterrows():
    an = row['Accession']
    name = row['Genome']
    accession_numbers.append(an)


#download_to_fasta(accession_numbers)

# %%
for index, row in virus_table.iterrows():
    an = row['Accession']
    name = row['Genome']
    accession_numbers.append(an)
    
    orf = genbank_orf_fetch(an, name, start_codon, stop_codons)
    virus_orfs.append(orf)

f = open("sars-orfs.fasta", "w")
for virus in virus_orfs:
    for i,orf in enumerate(virus.orfs[0:5]):
        f.write(">" + virus.name + " seq" + str(i) + "\r\n")
        line = str(orf['translation'])+"\r\n"
        f.write(line)
f.close()
